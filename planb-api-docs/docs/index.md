# PlanB portal API

This is document outlining the PlanB API for development of the refurbished PlanB portal.
This will be a fully REST API, that will receive and return JSON data. This API will not be customer facing, it will only 
be exposed to the react front end application.




## General Endpoints

This a table outline the endpoints that will be needed in the PlanB portal.

| Endpoint       | Description                              | Request Type |
|----------------|------------------------------------------|--------------|
| /authenticate         | Authenticate user. Validates credentials | `GET`        |
| /customer-all/{id} | Gets all information for customer [agents, customer, guests....] [SUPER ADMIN] | `GET` |
| /status/{id} | Gets a summary of status information relating to the provided id | `GET`       |


## Elements

This is how the elements of the plan B portal are connected

<img src="img/element-hierarchy.png" height="700" style="display: block;margin-left: auto;margin-right: auto"/>



## Customers

Customers are the users of the portal. Every logon to the portal will need an associated user. To create a user to the portal 
use the POST request to the `/customers` endpoint

| Endpoint       | Description                              | Request Type |
|----------------|------------------------------------------|--------------|
| /customers     | Gets all customers          | `GET`        |
| /customers      | Creates user                             | `POST`       |
| /customers/{id}      | Modifies user                            | `PUT`        |
| /customers/{id} | Gets users for provided id               | `GET`        |


## Appliances

I believe an appliance is the backup machine which is responsible for creating for services/agents for the agents assigned 
ot it.

Each customer has the possibility of having an appliance. This appliance is the central location as to where the 
agent/services are held. 

| Endpoint       | Description                              | Request Type |
|----------------|------------------------------------------|--------------|
| /appliances     | Gets all appliances [SUPER ADMIN]         | `GET`        |
| /appliances      | Creates appliance                             | `POST`       |
| /appliances/{id}      | Modifies appliance                            | `PUT`        |
| /appliances/{id} | Gets appliance for provided id               | `GET`        |


## Agents/Services

Agents/services are the target machines of which are being backed up. These machines may be phyiscal or virtual. Snapshots 
are taken of these machine. The data returned includes data about the machine and also it's snapshots


| Endpoint       | Description                              | Request Type |
|----------------|------------------------------------------|--------------|
| /appliances/{app_id}/agents     | Gets all agents [SUPER ADMIN]         | `GET`        |
| /appliances/{app_id}/agents      | Creates agent                             | `POST`       |
| /appliances/{app_id}/agents/{id}      | Modifies agent                            | `PUT`        |
| /appliances/{app_id}/agents/{id} | Gets agent for provided id               | `GET`        |



## History

History is time series data associated with agents/services on an appliance. The data returned is for all agents/services 
on a given appliance. There are a few variations o

| Endpoint       | Description                              | Request Type |
|----------------|------------------------------------------|--------------|
| /history/snapshot-retention     | Age of snapshots on the local appliance  | `POST`   |
| /history/snapshot-size| The uncompressed size of the most recent snapshot for each host.   | `POST`   |
| /history/snapshot-duration    | How long each snapshot takes  | `POST`    |
| /history/snapshot-push-duration| How long each snapshot takes to travel from the appliance to the server. | `POST` |
| /history/snapshot-push-size | How much data was actually sent to the Plan B servers.  | `POST`  |


