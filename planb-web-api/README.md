# Getting Started

### Prerequisites

- Python 3.8.2 or higher
- Up and running Redis client

### Project setup
```sh
# clone the repo
$ git clone https://.....
# move to the project folder
$ cd app
```

### Creating virtual environment

- Install `pipenv` a global python project `pip install pipenv`
- Create a `virtual environment` for this project
```shell
# creating pipenv environment for python 3
$ pipenv --three
# activating the pipenv environment
$ pipenv shell
# install all dependencies (include -d for installing dev dependencies)
$ pipenv install -d
```
### Configuration

- There are 3 configurations `development`, `staging` and `production` in `config.py`. Default is `development`
- Create a `.env` file from `.env.example` and set appropriate environment variables before running the project

### Running app

- Run flask app `python run.py`
- Logs would be generated under `log` folder

### Running celery workers

- Run redis locally before running celery worker
- Celery worker can be started with following command
```sh
# run following command in a separate terminal
$ celery worker -A celery_worker.celery -l=info
# (append `--pool=solo` for windows)
```

# Test
  Test if this app has been installed correctly and it is working via following curl commands (or use in Postman)
- Check if the app is running via `status` API
```shell
$ curl --location --request GET 'http://localhost:5000/status'
```
- Check if core app API and celery task is working via
```shell
$ curl --location --request GET 'http://localhost:5000/api/v1/core/test'
```
- Check if authorization is working via (change `API Key` as per you `.env`)
```shell
$ curl --location --request GET 'http://localhost:5000/api/v1/core/restricted' --header 'x-api-key: 436236939443955C11494D448451F'
```

# Dev environment
ref: https://www.how2shout.com/how-to/how-to-install-apache-mysql-php-phpmyadmin-on-windows-10-wsl.html

start database:
```sh
# start apache & mysql
$ sudo service apache2 start
$ sudo service mysql start
```

migrate database:
```sh
$ flask db migrate -m "changes to data structure migration."
$ flask db upgrade
```

# License
 This program is free software under MIT license. Please see the [LICENSE](LICENSE) file in our repository for the full text.