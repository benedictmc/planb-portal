import logging.config
from os import environ
import os

from celery import Celery
from dotenv import load_dotenv
from flask import Flask
from flask_cors import CORS
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_session import Session


from .config import config as app_config

db = SQLAlchemy()
migrate = Migrate()
celery = Celery(__name__)
ma = Marshmallow()
sess = Session()

def create_app():
    # loading env vars from .env file
    load_dotenv()
    APPLICATION_ENV = get_environment()
    logging.config.dictConfig(app_config[APPLICATION_ENV].LOGGING)
    app = Flask(app_config[APPLICATION_ENV].APP_NAME)
    app.config.from_object(app_config[APPLICATION_ENV])

    # TEMP in order to get session working
    # Should be saved in local env variable
    app.secret_key = '\xb8K\xad\xa7\xc5:\xe8\x1c\xe5]\x86\x15,;SK9V,\xfe9zS\xe6'
    app.config['SESSION_TYPE'] = 'filesystem'


    CORS(app)
    
    celery.config_from_object(app.config, force=True)
    # celery is not able to pick result_backend and hence using update
    celery.conf.update(result_backend=app.config['RESULT_BACKEND'])

    from .core.views import core as core_blueprint
    app.register_blueprint(
        core_blueprint,
        url_prefix='/api/v1'
    )

    from .portal.views import portal as portal_blueprint
    app.register_blueprint(
        portal_blueprint,
        url_prefix='/portal'
    )

    db.init_app(app)
    migrate.init_app(app, db)
    ma.init_app(app)
    sess.init_app(app)

    return app


def get_environment():
    return environ.get('APPLICATION_ENV') or 'development'
