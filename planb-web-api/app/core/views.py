
from flask import Blueprint, current_app, jsonify, request
from werkzeug.local import LocalProxy
from marshmallow import ValidationError


import app
from authentication import require_appkey
from .tasks import test_task

core = Blueprint('core', __name__)
logger = LocalProxy(lambda: current_app.logger)


@core.before_request
def before_request_func():
    current_app.logger.name = 'core'



@core.route('/', methods=['GET'])
def home():
    logger.info('app test route hit')
    # test_task.delay()
    return 'home'


@core.route('/test', methods=['GET'])
def test():
    logger.info('app test route hit')
    # test_task.delay()
    return 'Congratulations! Your core-app test route is running!'


@core.route('/restricted', methods=['GET'])
@require_appkey
def restricted():
    return 'Congratulations! Your core-app restricted route is running via your API key!'


@core.route('/db', methods=['GET'])
def db():
    try:
        app.db.session.execute('SELECT 1')
        return jsonify({'message':'DB connection working'}), 200
    except Exception as e:
        return jsonify({'message':'DB connection not working: '+str(e)}), 500
