

class DictToObject(object):
    """
        Creates an object with attributes from a dict

        Parameters
        ----------
            dictionary: dict

        Output
        ---------
            object with attributes for each key of the dict
            doesn't convert list of dicts to list of object
    """
    def __init__(self, dictionary):
        def _traverse(key, element):
            if isinstance(element, dict):
                return key, DictToObject(element)
            else:
                return key, element

        objd = dict(_traverse(k, v) for k, v in dictionary.items())
        self.__dict__.update(objd)



VALIDATION_ERROR = {
    "status": "failure",
    "message": "Input validation errors",
    'errors': None
}

"""
    Use to create the return error dict when an exception occurs on a view request.
    This is used to return more information with custom planB exception, while 
    also handling internal server errors.

    Parameters
    ----------
        e: Execption

    Output
    ---------
        error_dict: Dictionary
            Error dict which is returned to client with jsonify()
        status_code: Int
            Code of HTTP error
"""

def create_error_info(e):
    if type(e).__name__ == "PlanBError":
        if e.status_code == 400:
            error_dict = VALIDATION_ERROR
            error_dict['errors'] = e.message
            return error_dict, 400
        return {
            "status":"failure", 
            "status_code": e.status_code, 
            "message": e.message
        }, e.status_code
    else:
        return  {
            "status":"failure", 
            "status_code": 500, 
            "message": "A server error occurred, {}".format(str(e))
        }, 500


def form_validation_dict(errors):
    error_dict = VALIDATION_ERROR
    error_dict['errors'] = errors
    return error_dict