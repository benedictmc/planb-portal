# add your constants here
from urllib.error import HTTPError
import redis

class PlanBError(Exception):
    
    def __init__(self, status_code, message="Server error occured"):
        self.status_code = status_code
        self.message = message
        super().__init__(self.message)


STATUS_CODE_MESSAGE = {
    200 : PlanBError(200 , "Success"),
    401 : PlanBError(401 , "Unauthorized"),
    403 : PlanBError(403 , "Did not have access to resources"),
    500 : PlanBError(500 , "An internal server error occurred")
}

MASTER_AUTH_CREDENTIALS = {
    "username":"ben.mcgovern@ek.co", 
    "password":"g3R3h1"
}


REDIS = redis.Redis()



