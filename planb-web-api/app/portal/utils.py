import requests
from flask import session
import time
from .constants import * 
import uuid, random
import json
from collections import namedtuple
import traceback
# from .user import CURRENT_USER


class PlanbAPI(object):

    def __init__(self):
        self.JSESSION = None
        self.retrived = 0
        self.expiry = 0

    
    def get_jsession_auth(self, force_new=True):

        if int(time.time()) > self.expiry or force_new:
            auth_url = 'https://portal3.planb.co.uk/CustomerPortal/ajax/Login'
            res = requests.post(auth_url, verify=False, json=MASTER_AUTH_CREDENTIALS)
            if res.status_code == 200:
                self.JSESSION = res.cookies
                self.retrived = int(time.time())
                self.expiry = int(time.time())+1800
        return self.JSESSION


    def translate_error_code(self, code):
        return STATUS_CODE_MESSAGE[code]


    def check_response(self, response):
        status_code = response.status_code
        if response.status_code != 200:
            return False, None, status_code
        else:
            try:
                res_json = response.json()
                if not res_json['success']:
                    errors = res_json['error']
                    if "validation" in res_json.keys():
                        errors = self.__parse_validation_errors(res_json['validation'])
                    raise PlanBError(400 , errors)
                return True, res_json, status_code
            except ValueError:
                return False, None, status_code
            
    def __parse_validation_errors(self, validation_list):
        error_dict = {}
        for error in validation_list:
            error_dict[error['field']] = [error['error']]
        return error_dict


class CurrentUser():

    def __init__(self):
        self.logged_in = False
        self.uid = None
        self.cust_id = None
        self.name = None
        self.title = None
        self.surname = None
        self.email = None
        self.roles = None
        

    def create_user(self, user_details):
        if len(user_details['users']) > 1:
            raise("Multiple users with same email")
        self.logged_in = True
        user_json = user_details['users'][0]
        self.uid = user_json['uid']
        self.cust_id = user_json['customerId']
        self.name = user_json['givenName']
        self.surname = user_json['surname']
        self.email = user_json['email']
        self.title = user_json['title']
        self.roles = user_json['roles']
        user_login_uid = self.__create_session_hash()
        self.__add_to_redis(user_login_uid)
        return self


    def __create_session_hash(self):
        user_login_uid = uuid.uuid1()
        session['logged_in'] = str(user_login_uid)
        return user_login_uid


    def __add_to_redis(self, id):
        REDIS.set(str(id), json.dumps(self.user_details()))


    def __build_user(self, id):
        user_dict = json.loads(REDIS.get(id))
        for k, v in user_dict.items():
            setattr(self, k, v)


    def is_logged_in(self):
        logged_in_hash = session.get('logged_in')
        if logged_in_hash:
            self.__build_user(logged_in_hash)
            return True
        else:
            return False

    def has_session_hash(self):
        logged_in_hash = session.get('logged_in')
        if logged_in_hash:
            return True
        else:
            return False


    def user_details(self):
        return {
            "uid" : self.uid,
            "cust_id": self.cust_id,
            "name":  self.name,
            "title": self.title,
            "surname": self.surname,
            "email": self.email,
            "role": self.roles
        }

    def logout(self):
        self.logged_in = False
        self.uid = None
        self.cust_id = None
        self.name = None
        self.surname = None
        self.email = None
        self.title = None
        self.roles = None
        REDIS.delete(session['logged_in'])
        session.clear()



class UserUtils(PlanbAPI):

    def check_auth(self, email, password):
        current_user = CurrentUser()
        if current_user.has_session_hash():
            current_user.logout()
        try:
            auth_url = 'https://portal3.planb.co.uk/CustomerPortal/ajax/Login'
            res = requests.post(auth_url, verify=False,
                                json ={"username":email, "password":password})
            if res.status_code == 200:
                res_json = res.json()
                if res_json['username'] == email and res_json['success']:
                    return True
                else:
                    raise STATUS_CODE_MESSAGE[401]
        except Exception as e:
            print(traceback.format_exc())
            raise e
    
    def login_user(self, email):
        login_url = 'https://portal3.planb.co.uk/CustomerPortal/ajax/c/UserSearch'
        res = requests.post(login_url, cookies=super().get_jsession_auth(), 
                        json ={"filter":email}, verify=False )
        if res.status_code == 200:
            current_user = CurrentUser().create_user(res.json())
            return current_user


class Customers(PlanbAPI):

    def create_customer(self, form):
        customer = None
        customer_json = self.__create_customer_json(form)
        create_cust_url = 'https://portal3.planb.co.uk/CustomerPortal/ajax/c/UserSet'

        res = requests.post(create_cust_url, cookies=super().get_jsession_auth(),
                            json=customer_json,  verify=False)


        okay, customer, status_code = super().check_response(res)
        if okay:
            customer = res.json()
        else:
            # Error in request
            raise super().translate_error_code(status_code)
        return customer


    # SUPER ADMIN METHOD
    def get_all_customers(self):
        customers = []
        # Querys database for all customers
        # Return all
        return customers


    def get_customer(self, id):
        customer = None
        customer_url = 'https://portal3.planb.co.uk/CustomerPortal/ajax/c/UserSearch'
        res = requests.post(customer_url, cookies=super().get_jsession_auth(),
                            jso={"filter":'ben.mcgovern@ek.co'},  verify=False)
        # Querys database for customer with id as parameters
        # Return all
        return customer


    def modify_customer(self, id, form):
        customer = None
        # Modifies and querys database for customer with id as parameters
        # Return all
        return customer


    def __create_customer_json(self, form):
        uid = str(uuid.uuid4())
        # cust_id = random.randint(1000,9999)
        # Could possibly added code to add customer to dbs
        
        return {
	        "user": {
                # I think this is how you link the customer that is signed in 
                "customerId": "cust0001",
                "uid": uid,
                "email": form.email.data,
                "title": form.title.data,
                "givenName": form.forename.data,
                "surname": form.surname.data,
                "roles": [
                    # TODO
                    # Need to figure how roles are passed in 
                    "customer_portal_login",
                    "customer_portal_login_admin",
                    "appliance_portal_login",
                    "appliance_portal_retrieve_source",
                    "appliance_portal_retrieve_anywhere"
                ]
            },
        "isNewUser": True
        }

class Agents(PlanbAPI):

    def create_agent(self, form):
        agent = None
        # Creates agent with form details
        return agent


    # SUPER ADMIN METHOD
    def get_all_agents(self):
        agents = []
        # Querys database for all agents 
        # Return all
        return agents


    def get_agent(self, id):
        agent = None
        # Querys agent with agent id of parameter
        return agent

    
    def modify_agent(self, id, form):
        agent = None
        # Modifies and querys agent with agent id of parameter
        return agent


class Appliances(PlanbAPI):

    def create_appliance(self, form):
        appliance = None
        # Creates appliance with form details
        return appliance


    # SUPER ADMIN METHOD
    def get_all_appliances(self):
        appliances = []
        # Querys database for all appliances 
        # Return all
        return appliances


    def get_appliance(self, id):
        appliance = None
        appliance_url = 'https://portal3.planb.co.uk/CustomerPortal/ajax/c/Appliance'
        res = requests.post(appliance_url, cookies=super().get_jsession_auth(),
                            json ={"applianceId":id}, verify=False)
        status_code = res.status_code
        if status_code == 200:
            appliance = res.json()
        else:
            # For now should be an other type of error
            raise super().translate_error_code(status_code)
        return appliance

    
    def modify_appliance(self, id, form):
        appliance = None
        # Modifies and querys appliance with appliance id of parameter
        return appliance