from .utils import UserUtils, Customers, Agents, Appliances, CurrentUser
from functools import wraps
from .user import CURRENT_USER
from .constants import PlanBError
from flask import jsonify
from ..helpers import create_error_info

def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if CURRENT_USER.is_logged_in(): 
            return f(*args, **kwargs)
        else:
            e = PlanBError(403 , "Did not have access to resources")
            error_dict, status_code = create_error_info(e)
            return jsonify(error_dict), status_code

    return decorated_function
