
from flask import Blueprint, current_app, jsonify, request, session
from werkzeug.local import LocalProxy
from marshmallow import ValidationError
from ..forms.forms import * 
from .utils import UserUtils, Customers, Agents, Appliances, CurrentUser
from flask import session
from ..helpers import create_error_info, form_validation_dict
from .decorators import login_required
from .user import CURRENT_USER
from .constants import REDIS

import app
from authentication import require_appkey
from .tasks import test_task
import traceback
import redis

portal = Blueprint('portal', __name__)
logger = LocalProxy(lambda: current_app.logger)

# sess = Session()
# sess.init_app(app)



# Class set up
appliances_utils  = Appliances()
customer_utils = Customers()

user_utils = UserUtils()

@portal.before_request
def before_request_func():
    current_app.logger.name = 'portal'

# Used for development to see current logged in state
@portal.route('/', methods=['GET'])
def home():
    logger.info('app test route hit')
    if CURRENT_USER.is_logged_in():
        REDIS.get(session['logged_in'])
        login_status = "User logged in"
        data = CURRENT_USER.user_details()
    else:
        login_status = "User NOT logged in"
        data = None

    return jsonify({ "status":'status works, '+login_status, "data": data })

# Auth Method

@portal.route('/authenticate', methods=['POST'])
def authenticate():
    global CURRENT_USER
    try:
        form = AuthenticationForm(meta={'csrf': False})
        if not form.validate():
            error_dict = form_validation_dict(form.errors)
            return jsonify(error_dict), 400

        email = form.email.data
        password = form.password.data

        auth = user_utils.check_auth(email, password)
        if auth:
            # Sets user object for other requests
            CURRENT_USER = user_utils.login_user(email)
            return jsonify({"message": "successful", "data": CURRENT_USER.user_details()}), 200
    except Exception as e:
        print(traceback.format_exc())
        error_dict, status_code = create_error_info(e)
        return jsonify(error_dict), status_code
        return jsonify({"message": "A server error occurred, "+str(e)}), 500



@portal.route('/logout', methods=['GET'])
def logout():
    global CURRENT_USER
    try:
        CURRENT_USER.logout()
        return jsonify({"message": "successful"}), 200
    except Exception as e:
        return jsonify({"message": "A server error occurred, "+str(e)}), 500


# Customer Methods

# @portal.route('/customers', methods=['POST', 'GET'])
# def customers():
#     try:
#         if request.method == 'GET':
#             super_admin = True
#             if super_admin:
#                 return jsonify({"message": "successful", "data": Customers().get_all_customers() }), 200
#             else:
#                 return jsonify({"message": "not authorised"}), 401
#         if request.method == 'POST':   
#             form = CreateCustomerForm(meta={'csrf': False})
#             if not form.validate():
#                 error_dict = form_validation_dict(form.errors)
#                 return jsonify(error_dict), 400

#             new_customer = customer_utils.create_customer(form)
#             return jsonify({"message": "successful", "data": new_customer}), 200
#     except Exception as e:
#         print(traceback.format_exc())
#         error_dict, status_code = create_error_info(e)
#         return jsonify(error_dict), status_code



# @portal.route('/customers/<id>', methods=['GET', 'PUT'])
# def customer(id):
#     try:
#         if request.method == 'GET':
#             # check permissions
#             # Does logged in user have access to customer with id {id}
#             # Super user: yes 
#             # Admin: if in group
#             # Normal: if ownership
#             # if True:
#             #     user = UserUtils().dummy_auth()
#             #     print("Current user id is: ")
#             #     print(user.auth_cookie)
#             customer = Customers().get_customer(id)
            
#             if customer:
#                 return jsonify({"message": "success", "data": customer}), 200
#             else:
#                 return jsonify({"message": "failure", "data": "Could not retrieve customer"}), 200
#         if request.method == 'PUT':
#             form = ModifyCustomerForm(meta={'csrf': False})
#             if not form.validate():
#                 error_dict = form_validation_dict(form.errors)
#                 return jsonify(error_dict), 400

#             Customers().modify_customer(id, form)
#             # check permissions
#             # Does logged in user have access to customer with id {id}
#             # Super user: yes 
#             # Admin: if in group
#             # Normal: if ownership
#             return jsonify({"message": "success", "data": "customer"}), 200
#     except Exception as e:
#         print(traceback.format_exc())
#         return jsonify({"message": "A server error occurred, "+str(e)}), 500


# # Appliance Methods

# @portal.route('/appliances', methods=['GET', 'POST'])
# def appliances():
#     try:
#         if request.method == 'GET':
#             # check permissions
#             # Does logged in user have access to customer with id {id}
#             # Super user: yes 
#             # Admin: if in group
#             # Normal: if ownership
#             appliances = appliances.get_all_appliances()
#             return jsonify({"message": "success", "data": appliances}), 200
#         if  request.method == 'POST':
#             form = CreateApplianceForm(meta={'csrf': False})
#             if not form.validate():
#                 error_dict = form_validation_dict(form.errors)
#                 return jsonify(error_dict), 400

#             appliance = appliances().create_appliance(form)
#             return jsonify({"message": "success", "data": appliance}), 200
#     except Exception as e:
#         print(traceback.format_exc())
#         return jsonify({"message": "A server error occurred, "+str(e)}), 500



@portal.route('/appliances/<app_id>', methods=['GET', 'PUT'])
@login_required
def appliance(app_id):
    try:
        if request.method == 'GET':
            appliance = appliances_utils.get_appliance(app_id)
            if appliance:
                return jsonify({"message": "success", "data": appliance}), 200
            else:
                return jsonify({"message": "failure", "data": "Could not retrieve customer"}), 200
        if request.method == 'PUT':
            # check permissions
            # Does logged in user have access to customer with id {id}
            # Super user: yes 
            # Admin: if in group
            # Normal: if ownership
            form = ModifyApplianceForm(meta={'csrf': False})
            if not form.validate():
                error_dict = form_validation_dict(form.errors)
                return jsonify(error_dict), 400

            appliance = appliances.modify_appliance(id)
            return jsonify({"message": "success", "data": appliance}), 200
    except Exception as e:
        error_dict, status_code = create_error_info(e)
        return jsonify(error_dict), status_code


# Agent Methods

# @portal.route('/appliances/<app_id>/agents', methods=['GET', 'POST'])
# def agents(app_id):
#     try:
#         if request.method == 'GET':
#             # check permissions
#             # Does logged in user have access to customer with id {id}
#             # Super user: yes 
#             # Admin: if in group
#             # Normal: if ownership
#             agents = Agents.get_all_agents()
#             return jsonify({"message": "success", "data": agents}), 200
#         if  request.method == 'POST':
#             form = CreateAgentForm(meta={'csrf': False})
#             if not form.validate():
#                 error_dict = form_validation_dict(form.errors)
#                 return jsonify(error_dict), 400

#             agent = Agents().create_agent(form)
#             return jsonify({"message": "success", "data": agent}), 200
#     except Exception as e:
#         print(traceback.format_exc())
#         return jsonify({"message": "A server error occurred, "+str(e)}), 500



# @portal.route('/appliances/<app_id>/agents/<agent_id>', methods=['GET', 'PUT'])
# def agent(app_id, agent_id):
#     try:
#         if request.method == 'GET':
#             # check permissions
#             # Does logged in user have access to customer with id {id}
#             # Super user: yes 
#             # Admin: if in group
#             # Normal: if ownership
#             agent = Agents.get_agent(id)
#             return jsonify({"message": "success", "data": agent}), 200        
#         if request.method == 'PUT':
#             # check permissions
#             # Does logged in user have access to customer with id {id}
#             # Super user: yes 
#             # Admin: if in group
#             # Normal: if ownership
#             form = ModifyAgentForm(meta={'csrf': False})
#             if not form.validate():
#                 error_dict = form_validation_dict(form.errors)
#                 return jsonify(error_dict), 400

#             agent = Agents.modify_agent(id)
#             return jsonify({"message": "success", "data": agent}), 200
#     except Exception as e:
#         print(traceback.format_exc())
#         return jsonify({"message": "A server error occurred, "+str(e)}), 500


 
    
