from flask_wtf import FlaskForm
from wtforms import BooleanField, StringField, PasswordField
from wtforms.validators import DataRequired, Email, Optional, Length, Regexp, \
                                URL, ValidationError

def validate_password_repeat(form, field):
    if field.data != form.password.data:
        raise ValidationError('Passwords must match')


def validate_title(form, field):
    if field.data not in ["Mr.", "Mrs.", "Miss"]:
        raise ValidationError('Title not correct')


class AuthenticationForm(FlaskForm):

    email = StringField('email', [ DataRequired() ])
    password = PasswordField('password', [ DataRequired() ])


class CreateCustomerForm(FlaskForm):

    forename = StringField('forename', validators=[DataRequired()])
    surname = StringField('surname', validators=[DataRequired()])
    title = StringField('title', validators=[DataRequired(), validate_title])
    email = StringField('email', validators=[DataRequired(), Email()])
    password = StringField('password', validators=[DataRequired()])
    passverify = StringField('passverify', validators=[
        validate_password_repeat
    ])
    roles = StringField('roles', validators=[DataRequired()])
    

class ModifyCustomerForm(FlaskForm):
    
    # SAME AS CREATE FOR NOW
    email = StringField('email', validators=[DataRequired(), Email()])
    password = StringField('password', validators=[DataRequired()])
    passverify = StringField('passverify', validators=[
        validate_password_repeat
    ])


class CreateAgentForm(FlaskForm):

    # SAME AS CREATE FOR NOW
    email = StringField('email', validators=[DataRequired(), Email()])
    password = StringField('password', validators=[DataRequired()])
    passverify = StringField('passverify', validators=[
        validate_password_repeat
    ])
    

class ModifyAgentForm(FlaskForm):
    
    # SAME AS CREATE FOR NOW
    email = StringField('email', validators=[DataRequired(), Email()])
    password = StringField('password', validators=[DataRequired()])
    passverify = StringField('passverify', validators=[
        validate_password_repeat
    ])


class CreateApplianceForm(FlaskForm):

    # SAME AS CREATE FOR NOW
    email = StringField('email', validators=[DataRequired(), Email()])
    password = StringField('password', validators=[DataRequired()])
    passverify = StringField('passverify', validators=[
        validate_password_repeat
    ])
    

class ModifyApplianceForm(FlaskForm):
    
    # SAME AS CREATE FOR NOW
    email = StringField('email', validators=[DataRequired(), Email()])
    password = StringField('password', validators=[DataRequired()])
    passverify = StringField('passverify', validators=[
        validate_password_repeat
    ])