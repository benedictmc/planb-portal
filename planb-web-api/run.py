from app import create_app
import os 

# Python script to start application
app = create_app()


@app.route('/status', methods=['GET'])
def status():
    return 'Running!'

host='0.0.0.0'

if __name__ == '__main__':
    app.run(host=host)
